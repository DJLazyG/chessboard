//defining the dimentions
let boardSizeX = 8;
let boardSizeY = 6;

//creating an empty string to store the result
let result = '';

//loop to create rows
for(i=0;i<boardSizeY;i++){
    //loop to create columns
    for(z=0;z<boardSizeX;z++){
        //Conditional statement
        if((i + z) % 2 === 0){
            result = result + "O";
        }else{
            result = result + "X";
        }
    }
    result = result +"\n";
}
window.alert(result);
console.log(result);